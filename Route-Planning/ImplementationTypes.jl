module ImplementationTypes

export State
export BezierPath, buildBezier, buildBezierCoefs
export Tour

mutable struct State
	node::Int64
	orientation::Int64
end

Base.isequal(s1::State, s2::State) = s1.node == s2.node && s1.orientation == s2.orientation
Base.hash(s::State, seed::Int64) = s.node + seed*s.orientation
Base.hash(s::State) = hash(s, 1000)
Base.copy(s::State) = State(s.node, s.orientation)
Base.in(s::State, sc::Array{State}) = any(isequal(s, si) for si in sc)
Base.isequal(t1::Tuple{State,State,Int64}, t2::Tuple{State,State,Int64}) = isequal(t1[1], t2[1]) && isequal(t1[2], t2[2]) && isequal(t1[3], t2[3])
Base.hash(t::Tuple{State,State,Int64}) = hash(t[1]) + hash(t[2]) + hash(t[3])
Base.string(s::State) = string("(", s.node, ", ", s.orientation, ")")
Base.show(s::State) = show(string(s))
function Base.print(s::State)
	print("(", s.node, ", ", s.orientation, ")")
end
function Base.println(s::State)
	println("(", s.node, ", ", s.orientation, ")")
end

mutable struct BezierPath
	Px::Function
	Py::Function
	Vx::Function
	Vy::Function
	Ax::Function
	Ay::Function
end

function buildBezierCoefs(startState::State, endState::State, minspeed::Float64, Positions, Orientations; skewness=(1.0, 1.0))
	a0 = Positions[startState.node, 1]
	b0 = Positions[startState.node, 2]
	a1 = cos(Orientations[startState.orientation])*minspeed*skewness[1]
	b1 = sin(Orientations[startState.orientation])*minspeed*skewness[1]
	Px1 = Positions[endState.node, 1]
	Py1 = Positions[endState.node, 2]
	Vx1 = cos(Orientations[endState.orientation])*minspeed*skewness[2]
	Vy1 = sin(Orientations[endState.orientation])*minspeed*skewness[2]
	a3 = Vx1 - a1 - 2.0*(Px1 - a1 - a0)
	b3 = Vy1 - b1 - 2.0*(Py1 - b1 - b0)
	a2 = Px1 - a3 - a1 - a0
	b2 = Py1 - b3 - b1 - b0
	return a3, a2, a1, a0, b3, b2, b1, b0
end

function buildBezierCoefs(startState::State, endPos::Array{Float64, 1}, endAngle::Float64, minspeed::Float64, Positions, Orientations; skewness=(1.0, 1.0))
	a0 = Positions[startState.node, 1]
	b0 = Positions[startState.node, 2]
	a1 = cos(Orientations[startState.orientation])*minspeed*skewness[1]
	b1 = sin(Orientations[startState.orientation])*minspeed*skewness[1]
	Px1 = endPos[1]
	Py1 = endPos[2]
	Vx1 = cos(endAngle)*minspeed*skewness[2]
	Vy1 = sin(endAngle)*minspeed*skewness[2]
	a3 = Vx1 - a1 - 2.0*(Px1 - a1 - a0)
	b3 = Vy1 - b1 - 2.0*(Py1 - b1 - b0)
	a2 = Px1 - a3 - a1 - a0
	b2 = Py1 - b3 - b1 - b0
	return a3, a2, a1, a0, b3, b2, b1, b0
end

function buildBezierCoefs(startPos::Array{Float64, 1}, startAngle::Float64, endState::State, minspeed::Float64, Positions, Orientations; skewness=(1.0, 1.0))
	a0 = startPos[1]
	b0 = startPos[2]
	a1 = cos(startAngle)*minspeed*skewness[1]
	b1 = sin(startAngle)*minspeed*skewness[1]
	Px1 = Positions[endState.node, 1]
	Py1 = Positions[endState.node, 2]
	Vx1 = cos(Orientations[endState.orientation])*minspeed*skewness[2]
	Vy1 = sin(Orientations[endState.orientation])*minspeed*skewness[2]
	a3 = Vx1 - a1 - 2.0*(Px1 - a1 - a0)
	b3 = Vy1 - b1 - 2.0*(Py1 - b1 - b0)
	a2 = Px1 - a3 - a1 - a0
	b2 = Py1 - b3 - b1 - b0
	return a3, a2, a1, a0, b3, b2, b1, b0
end

function buildBezier(startState::State, endState::State, minspeed::Float64, Positions, Orientations; skewness=(1.0, 1.0))
	a0 = Positions[startState.node, 1]
	b0 = Positions[startState.node, 2]
	a1 = cos(Orientations[startState.orientation])*minspeed*skewness[1]
	b1 = sin(Orientations[startState.orientation])*minspeed*skewness[1]
	Px1 = Positions[endState.node, 1]
	Py1 = Positions[endState.node, 2]
	Vx1 = cos(Orientations[endState.orientation])*minspeed*skewness[2]
	Vy1 = sin(Orientations[endState.orientation])*minspeed*skewness[2]
	a3 = Vx1 - a1 - 2.0*(Px1 - a1 - a0)
	b3 = Vy1 - b1 - 2.0*(Py1 - b1 - b0)
	a2 = Px1 - a3 - a1 - a0
	b2 = Py1 - b3 - b1 - b0
	Px(t) = a3*t*t*t + a2*t*t + a1*t + a0
	Py(t) = b3*t*t*t + b2*t*t + b1*t + b0
	Dx(t) = (3*a3*t*t + 2*a2*t + a1)
	Dy(t) = (3*b3*t*t + 2*b2*t + b1)
	DDx(t) = 6*a3*t + 2*a2
	DDy(t) = 6*b3*t + 2*b2
	Dnorm(t) = sqrt(Dx(t)*Dx(t) + Dy(t)*Dy(t))
	Vx(t) = Dx(t)/Dnorm(t)*minspeed
	Vy(t) = Dy(t)/Dnorm(t)*minspeed
	Ax(t) = (DDx(t)/Dnorm(t) - Dx(t)*(DDx(t)*Dx(t)+DDy(t)*Dy(t))/Dnorm(t)*Dnorm(t)*Dnorm(t))*minspeed
	Ay(t) = (DDy(t)/Dnorm(t) - Dy(t)*(DDx(t)*Dx(t)+DDy(t)*Dy(t))/Dnorm(t)*Dnorm(t)*Dnorm(t))*minspeed
	return BezierPath(Px, Py, Vx, Vy, Ax, Ay)
end

function buildBezier(startState::State, endPos::Array{Float64, 1}, endAngle::Float64, minspeed::Float64, Positions, Orientations; skewness=(1.0, 1.0))
	a0 = Positions[startState.node, 1]
	b0 = Positions[startState.node, 2]
	a1 = cos(Orientations[startState.orientation])*minspeed*skewness[1]
	b1 = sin(Orientations[startState.orientation])*minspeed*skewness[1]
	Px1 = endPos[1]
	Py1 = endPos[2]
	Vx1 = cos(endAngle)*minspeed*skewness[2]
	Vy1 = sin(endAngle)*minspeed*skewness[2]
	a3 = Vx1 - a1 - 2.0*(Px1 - a1 - a0)
	b3 = Vy1 - b1 - 2.0*(Py1 - b1 - b0)
	a2 = Px1 - a3 - a1 - a0
	b2 = Py1 - b3 - b1 - b0
	Px(t) = a3*t*t*t + a2*t*t + a1*t + a0
	Py(t) = b3*t*t*t + b2*t*t + b1*t + b0
	Dx(t) = (3*a3*t*t + 2*a2*t + a1)
	Dy(t) = (3*b3*t*t + 2*b2*t + b1)
	DDx(t) = 6*a3*t + 2*a2
	DDy(t) = 6*b3*t + 2*b2
	Dnorm(t) = sqrt(Dx(t)*Dx(t) + Dy(t)*Dy(t))
	Vx(t) = Dx(t)/Dnorm(t)*minspeed
	Vy(t) = Dy(t)/Dnorm(t)*minspeed
	Ax(t) = (DDx(t)/Dnorm(t) - Dx(t)*(DDx(t)*Dx(t)+DDy(t)*Dy(t))/Dnorm(t)*Dnorm(t)*Dnorm(t))*minspeed
	Ay(t) = (DDy(t)/Dnorm(t) - Dy(t)*(DDx(t)*Dx(t)+DDy(t)*Dy(t))/Dnorm(t)*Dnorm(t)*Dnorm(t))*minspeed
	return BezierPath(Px, Py, Vx, Vy, Ax, Ay)
end

function buildBezier(startPos::Array{Float64, 1}, startAngle::Float64, endState::State, minspeed::Float64, Positions, Orientations; skewness=(1.0, 1.0))
	a0 = startPos[1]
	b0 = startPos[2]
	a1 = cos(startAngle)*minspeed*skewness[1]
	b1 = sin(startAngle)*minspeed*skewness[1]
	Px1 = Positions[endState.node, 1]
	Py1 = Positions[endState.node, 2]
	Vx1 = cos(Orientations[endState.orientation])*minspeed*skewness[2]
	Vy1 = sin(Orientations[endState.orientation])*minspeed*skewness[2]
	a3 = Vx1 - a1 - 2.0*(Px1 - a1 - a0)
	b3 = Vy1 - b1 - 2.0*(Py1 - b1 - b0)
	a2 = Px1 - a3 - a1 - a0
	b2 = Py1 - b3 - b1 - b0
	Px(t) = a3*t*t*t + a2*t*t + a1*t + a0
	Py(t) = b3*t*t*t + b2*t*t + b1*t + b0
	Dx(t) = (3*a3*t*t + 2*a2*t + a1)
	Dy(t) = (3*b3*t*t + 2*b2*t + b1)
	DDx(t) = 6*a3*t + 2*a2
	DDy(t) = 6*b3*t + 2*b2
	Dnorm(t) = sqrt(Dx(t)*Dx(t) + Dy(t)*Dy(t))
	Vx(t) = Dx(t)/Dnorm(t)*minspeed
	Vy(t) = Dy(t)/Dnorm(t)*minspeed
	Ax(t) = (DDx(t)/Dnorm(t) - Dx(t)*(DDx(t)*Dx(t)+DDy(t)*Dy(t))/Dnorm(t)*Dnorm(t)*Dnorm(t))*minspeed
	Ay(t) = (DDy(t)/Dnorm(t) - Dy(t)*(DDx(t)*Dx(t)+DDy(t)*Dy(t))/Dnorm(t)*Dnorm(t)*Dnorm(t))*minspeed
	return BezierPath(Px, Py, Vx, Vy, Ax, Ay)
end


mutable struct Tour
	states::Array{State, 1}
	a::Int64
end

Base.copy(t::Tour) = Tour(copy(t.states), copy(t.a))
Base.deepcopy(t::Tour) = Tour(deepcopy(t.states), deepcopy(t.a))
Base.start(t::Tour) = start(t.states)
Base.next(t::Tour, state) = next(t.states, state)
Base.done(t::Tour, state) = done(t.states, state)
Base.length(t::Tour) = length(t.states)
Base.getindex(t::Tour, index) = getindex(t.states, index)
Base.setindex!(t::Tour, X, index) = setindex!(t.states, X, index)
Base.eachindex(t::Tour) = eachindex(t.states)
Base.view(t::Tour, index) = view(t.states, index)
Base.endof(t::Tour) = length(t.states)
#Base.hash(t::Tour, seed::Int64) = sum(s.node + seed*s.orientation for s in t.states)  + seed*seed*t.a
Base.hash(t::Tour, seed::Int64) = sum(s.node for s in t.states)  + seed*t.a
Base.hash(t::Tour) = hash(t, 1000)
Base.deleteat!(t::Tour, pos) = deleteat!(t.states, pos)
function Base.isequal(t1::Tour, t2::Tour)
	return hash(t1) == hash(t2)
	#if t1.a != t2.a || length(t1.states) != length(t2.states)
		#return false
	#end
	#for i in t1.states
		#if !(i in t2.states)
			#return false
		#end
	#end
	#return true
end
Base.append!(t::Tour, s) = append!(t.states, s)
Base.push!(t::Tour, s::State) = push!(t.states, s)

#Base.hash(tours::Array{Tour,1}, seed::Int) = sum(hash(t,seed) for t in tours)
#Base.hash(tours::Array{Tour,1}) = hash(tours, 1000)
Base.isequal(tours1::Array{Tour,1}, tours2::Array{Tour,1}) = hash(tours1) == hash(tours2)

end
