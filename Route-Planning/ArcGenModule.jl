module OrderGA

using ImplementationTypes

type TourEntity <: Entity
    tour::Tour
    fitness

    TourEntity() = new(Tour(State[], 1), nothing)
    TourEntity(tour) = new(tour, nothing)
end	

function create_entity(num)
    # for simplicity sake, let's limit the values for abcde to be integers in [-42, 42]
    TourEntity(rand(Int, 5) % 43)
end

end
