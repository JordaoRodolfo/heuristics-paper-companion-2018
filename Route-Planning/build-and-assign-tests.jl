#!/usr/bin/env julia
if !(pwd() in LOAD_PATH)
  push!(LOAD_PATH, pwd())
end

using ImplementationTypes
using BuildStage
using AssignStage
#using Plots
using PGFPlots
using ColorTypes
using ColorBrewer
using ArgParse
using YAML
using Utils
#using Cbc
#solver = CbcSolver()

function dict2plot(d)
  B = zeros(length(d)+1, 2)
  for i in Iterators.take(eachindex(B), length(d))
    B[i, :] = d[i]
  end
  B[length(d)+1, :] = d[1]
  return B
end

function makeSolutionPlots(specs, buildDict, assignDict, sol, outName)
  boundaries = specs["Boundaries"]
  if haskey(specs, "Obstacles")
    obstacles = specs["Obstacles"]
  else
    obstacles = Dict()
  end
  interest = buildDict["Interest"]
  interest_nodes = buildDict["InterestNodes"]
  pos = buildDict["Positions"]
  base_pos = specs["Base"]["pos"]
  base_angle = specs["Base"]["angle"]
  base_inbound = buildDict["BaseInbound"]
  base_outbound = buildDict["BaseOutbound"]
  allpaths = buildDict["Paths"]
  agents = specs["Agents"]
  cover_radius = specs["CoverRadius"]
  orientations = buildDict["Orientations"]
  invrelation = Dict(v => k for (k, v) in assignDict["Relation"])
  colours = Dict(1 => "red", 2 => "blue")
  B = dict2plot(boundaries)
  plotregion = Axis(xlabel="X [space unit]", ylabel="Y [space unit]", width="400pt", height="400pt", axisEqual=true)
  plotregion = Axis(xlabel="X [space unit]", ylabel="Y [space unit]", width="0.98\\textwidth", height="0.95\\textheight", axisEqual=true)

  push!(plotregion.plots, Plots.Linear(B[:, 1], B[:, 2], style="mark=none, color=black"))
  for (n, area) in obstacles
    B = dict2plot(area["Boundaries"])
    push!(plotregion.plots, Plots.Linear(B[:, 1], B[:, 2], style="mark=none, color=black"))
  end
  for (n, area) in interest
    B = dict2plot(area["Boundaries"])
    push!(plotregion.plots, Plots.Linear(B[:, 1], B[:, 2], style="mark=none, dotted, color=black"))
  end
  push!(plotregion.plots, Plots.Node(L"b^{R}", base_pos[1], base_pos[2], style="above left"))
  push!(plotregion.plots, Plots.Node("", base_pos[1], base_pos[2], style="draw, circle, inner sep=0.01cm, minimum size=0.01cm, blue"))
  plotsol = deepcopy(plotregion)
  colors = palette("Set1", 9)
  lines = function(n)
    style = "dash pattern="
    if n > 1
      style = string(style, "on 5pt off 2pt ")
      for i in 1:n-2
        style = string(style, "on 1pt off 1pt ")
      end
      style = string(style, "on 1pt off 2pt")
    end
    return style
  end
  agents_name= Dict(1 => "UGV", 2 => "UAV")
  ntour = 0
  for tour in sol
    ncolor = rem(ntour, length(colors)) + 1
    ntour += 1
    a = tour.a
    v_red = convert(Float64, red(colors[ncolor]))
    v_green = convert(Float64, green(colors[ncolor]))
    v_blue = convert(Float64, blue(colors[ncolor]))
    color = "{rgb:red,$v_red; green,$v_green; blue,$v_blue}"
    line = lines(ntour)
    for i in Iterators.take(eachindex(tour), length(tour)-1)
      s = State(invrelation[tour.states[i].node], tour.states[i].orientation)
      e = State(invrelation[tour.states[i+1].node], tour.states[i+1].orientation)
      b = first(allpaths[s.node, e.node, s.orientation, e.orientation, a])
      for f in Iterators.drop(allpaths[s.node, e.node, s.orientation, e.orientation, a], 1)
        p = buildBezier(b, f, agents[a]["minspeed"], pos, orientations)
        X = [p.Px(t) for t in linspace(0.0, 1.0, 10)]
        Y = [p.Py(t) for t in linspace(0.0, 1.0, 10)]
        plt = Plots.Linear(X, Y, style="$line, ->, mark=none, color=$color")
        push!(plotsol.plots, plt)
        b = f
      end
    end
    name = agents_name[a]
    insert!(plotsol.plots, 2*ntour-1, Plots.Command("\\addlegendimage{$line, color=$color}"))
    insert!(plotsol.plots, 2*ntour, Plots.Command("\\addlegendentry{Tour $ntour, Agent $name}"))
  end
  base_angle = base_angle/180*pi
  for a in 1:length(agents)
    bi = buildDict["BaseInbound"][a]
    bo = buildDict["BaseOutbound"][a]
    p = buildBezier(base_pos, base_angle, bi, agents[a]["minspeed"], pos, orientations)
    push!(plotsol, Plots.Linear([p.Px(t) for t in linspace(0.0, 1.0, 10)], [p.Py(t) for t in linspace(0.0, 1.0, 10)], style=s"solid, mark=none, color=black"))
    p = buildBezier(bo, base_pos, mod(base_angle+pi, 2*pi), agents[a]["minspeed"], pos, orientations)
    push!(plotsol, Plots.Linear([p.Px(t) for t in linspace(0.0, 1.0, 10)], [p.Py(t) for t in linspace(0.0, 1.0, 10)], style=s"solid, mark=none, color=black"))
  end

  save("$outName.tex", plotsol, include_preamble=false)
end

solver = nothing
try
  using Gurobi
  solver = GurobiSolver(TimeLimit=3600.0)
catch
  using GLPKMathProgInterface
  solver = GLPKSolverMIP()
end


#gr()
# pyplot()
#plotlyjs()

s = ArgParseSettings()

@add_arg_table s begin
  "--non-grid"
  help = "Generate instance in a grid, instead of free."
  action = :store_true
  "--plot-all"
  help = "Plot all intermediate steps"
  action = :store_true
  "--exact"
  help = "Solve via MILP"
  action = :store_true
  "--localMethod"
  help = "Local Method optim. to use. (Default: ReSS)"
  arg_type = String
  default = "InO"
  "Specs"
  help = "Name of specs file for generation"
  arg_type = String
  default = "situation7-specs.yaml"
  "-l", "--label"
  help = "Label given to files of output"
  arg_type = String
  default = "results"
end

args = parse_args(s)

specsfiles = ["situation1-specs.yaml", "situation5-specs.yaml", "situation6-specs.yaml", "limited-real-region-situation.yaml", "real-region-situation.yaml"]
bestResults = Dict{String, Float64}()

for specfile in specsfiles
  name = first(split(specfile, "-"))
  results = Dict("$name-exact-result" => zeros(0, 2), "$name-OsS-result" => zeros(0, 2), "$name-CoS-result" => zeros(0, 2))

  println("Reading specs...")

  output_name = args["label"]
  specs = YAML.load(open(specfile))

  buildDict = @time buildStage(specs, verbose=true)

  println("Building model...")

  assignDict = @time buildModel(buildDict)

  println("Solving...")

  for t in 1:1000
    #sol, val, iters, optim_iters = @time ASC2(assignDict["StartStates"], assignDict["EndStates"], assignDict["Orientations"],
    #assignDict["NodeCosts"], assignDict["Costs"], assignDict["Demand"], assignDict["Supply"], assignDict["Betas"],
    #20, 200, CiROptim, 300, printStep=1)
    #if !args["exact"]
    #if args["localMethod"] == "CiR"
    #sol, val, iters, optim_iters = @time ASC(assignDict["StartStates"], assignDict["EndStates"], assignDict["Orientations"],
    #assignDict["NodeCosts"], assignDict["Costs"], assignDict["Demand"], assignDict["Supply"], assignDict["Betas"],
    #20, 200, CiROptim, 300, printStep=5)
    #elseif args["localMethod"] == "InO"
    #sol, val, iters, optim_iters = @time ASC(assignDict["StartStates"], assignDict["EndStates"], assignDict["Orientations"],
    #assignDict["NodeCosts"], assignDict["Costs"], assignDict["Demand"], assignDict["Supply"], assignDict["Betas"],
    #20, 200, InOOptim, 300, printStep=5)
    #elseif args["localMethod"] == "OsS"
    gc()
    ret, time, _, _ = @timed ASC(assignDict["StartStates"], assignDict["EndStates"], assignDict["Orientations"],
                                 assignDict["NodeCosts"], assignDict["Costs"], assignDict["Demand"], assignDict["Supply"], assignDict["Betas"],
                                 OsSOptim, maxASCIters=300, maxASCRepeats=10, maxRepeats=10, maxIters=100, printStep=20, maxTime=3600)
    sol, val, iters, optim_iters = ret
    results["$name-OsS-result"] = [results["$name-OsS-result"]; [val["total"] time]]
    println("Time: $time")
    println("Solved. ASC Iters: ", iters, " OPTIM Iters: ", optim_iters, " Val: ", val)
    if haskey(bestResults, "$name-OsS-result") == false || bestResults["$name-OsS-result"] > val["total"]
      bestResults["$name-OsS-result"] = val["total"]
      makeSolutionPlots(specs, buildDict, assignDict, sol, "visual-$name-OsS-best-result")
    end
    #elseif args["localMethod"] == "CoS"
    gc()
    ret, time, _, _ = @timed ASC(assignDict["StartStates"], assignDict["EndStates"], assignDict["Orientations"],
                                 assignDict["NodeCosts"], assignDict["Costs"], assignDict["Demand"], assignDict["Supply"], assignDict["Betas"],
                                 CoSOptim, maxASCIters=300, maxASCRepeats=10, maxRepeats=10, maxIters=100, printStep=20, maxTime=3600)
    sol, val, iters, optim_iters = ret
    results["$name-CoS-result"] = [results["$name-CoS-result"]; [val["total"] time]]
    println("Time: $time")
    println("Solved. ASC Iters: ", iters, " OPTIM Iters: ", optim_iters, " Val: ", val)
    println(sol)
    if haskey(bestResults, "$name-CoS-result") == false || bestResults["$name-CoS-result"] > val["total"]
      bestResults["$name-CoS-result"] = val["total"]
      makeSolutionPlots(specs, buildDict, assignDict, sol, "visual-$name-CoS-best-result")
    end
    #elseif args["localMethod"] == "Gen"
    #sol, val, iters, optim_iters = @time ASC(assignDict["StartStates"], assignDict["EndStates"], assignDict["Orientations"],
    #assignDict["NodeCosts"], assignDict["Costs"], assignDict["Demand"], assignDict["Supply"], assignDict["Betas"],
    #GenOptim, maxASCIters=1000, maxASCRepeats=10, maxRepeats=20, maxIters=2000, printStep=20)
    #end
  end
  for t in 1:100
    #else
    gc()
    ret, time, _, _ = @timed MILP(assignDict["StartStates"], assignDict["EndStates"], assignDict["Orientations"],
                                  assignDict["NodeCosts"], assignDict["Costs"], assignDict["Demand"], assignDict["Supply"], assignDict["Betas"],
                                  solver)
    sol, val, iters, optim_iters = ret
    if haskey(bestResults, "$name-exact-result") == false || bestResults["$name-exact-result"] > val["total"]
      bestResults["$name-exact-result"] = val["total"]
      makeSolutionPlots(specs, buildDict, assignDict, sol, "visual-$name-exact-best-result")
    end
    results["$name-exact-result"] = [results["$name-exact-result"]; [val["total"] time]]
    println("Time: $time")
    println("Solved. ASC Iters: ", iters, " OPTIM Iters: ", optim_iters, " Val: ", val)
  end

  for (k, v) in results
    writecsv(open(string(k, ".csv"), "a"), v)
  end

end

