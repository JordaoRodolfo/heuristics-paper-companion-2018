module BuildStage

export buildStage, integrateEffort

using ImplementationTypes
using Cubature
using ProgressMeter

function integrateLine(Fun, xmin, xmax; strips=5, speed=1)
	total = 0.0
	step = vec(xmax - xmin)/strips
	vel = speed*normalize(xmax - xmin)
	p = xmin
	for i in 1:strips
		a = (i-1)*step + xmin
		ab2 = (i-0.5)*step + xmin
		b = i*step + xmin
		total += Fun(a[1], a[2], vel[1], vel[2])
		total += 4*Fun(ab2[1], ab2[2], vel[1], vel[2])
		total += Fun(b[1], b[2], vel[1], vel[2])
		total *= norm(b-a, 2)/6
	end
	return total
end

function integrateEffort(Effort, pathBezier::BezierPath, obstacles, boundaries; reltol=1e-3, checkpoints=15)
	U(t) = Effort(pathBezier.Px(t), pathBezier.Py(t), pathBezier.Vx(t), pathBezier.Vy(t), pathBezier.Ax(t), pathBezier.Ay(t))
	if !all(t -> checkInside(pathBezier.Px(t), pathBezier.Py(t), boundaries), linspace(0.0, 1.0, checkpoints))
		return Inf, 0.0
	elseif any(t -> any(obs -> checkInside(pathBezier.Px(t), pathBezier.Py(t), obs), obstacles), linspace(0.0, 1.0, checkpoints))
		return Inf, 0.0
	end
	return hquadrature(U, 0, 1, reltol=reltol)
end

function computePathsEndToEnd(start::State, finish::State, neighs::Dict{State, Array{State, 1}}, costs::Array{Float64, 4}, paths::Array{Array{State, 1}, 4})
	println(start)
	println(finish)
	if start == finish
		return 0.0, State[]
	else
    	if costs[start.node, finish.node, start.orientation, finish.orientation] == Inf
    		best = costs[start.node, finish.node, start.orientation, finish.orientation]
    		for neigh in neighs[start]
    			if costs[neigh.node, finish.node, neigh.orientation, finish.orientation] == Inf
    				costs[neigh.node, finish.node, neigh.orientation, finish.orientation], paths[neigh.node, finish.node, neigh.orientation, finish.orientation] = computePathsEndToEnd(neigh, finish, neighs, costs, paths)
    			end
    			if costs[start.node, neigh.node, start.orientation, neigh.orientation] + costs[neigh.node, finish.node, neigh.orientation, finish.orientation] < best
                    best = costs[start.node, neigh.node, start.orientation, neigh.orientation] + costs[neigh.node, finish.node, neigh.orientation, finish.orientation]
    				costs[start.node, finish.node, start.orientation, finish.orientation] = best
    				paths[start.node, finish.node, start.orientation, finish.orientation] = [start, paths[neigh.node, finish.node, neigh.orientation, finish.orientation]]
    			end
    		end
    	end
    	return costs[start.node, finish.node, start.orientation, finish.orientation], paths[start.node, finish.node, start.orientation, finish.orientation]
	end
end

function computePathsDirect(neighs::Dict{State, Array{State, 1}}, costs::Array{Float64, 4}, interest_nodes::Array{Int64, 1})
	N = size(costs, 1)
	M = size(costs, 3)
	states = [State(n,phi) for n in 1:N for phi in 1:M if length(neighs[State(n, phi)]) > 0]
	filter!(s -> any(s in v for (k,v) in neighs), states)
	goals = filter(g -> g.node in interest_nodes, states)
	next = Dict(state => Dict(state => state) for state in states)
	paths = fill(State[], (N,N,M,M))
	@showprogress 1 "PATH FIND: Building neighs for $N states..." for state in states
		for neigh in neighs[state]
			next[state][neigh] = neigh
		end
	end
	@showprogress 1 "PATH FIND: Computing paths for $N states..." for k in states
		for i in states
    		for j in states
				if i != j
                    before = costs[i.node, j.node, i.orientation, j.orientation]
    				possible = costs[i.node, k.node, i.orientation, k.orientation] + costs[k.node, j.node, k.orientation, j.orientation]
    				if possible < before
        				costs[i.node, j.node, i.orientation, j.orientation] = possible
    					next[i][j] = next[i][k]
    				end
				end
    		end
		end
	end
	@showprogress 1 "PATH FIND: Getting paths in list form..." for i in goals
		for j in goals
			if i != j
    			path = [i]
    			k = next[i][j]
    			while k != j && k != i
    				push!(path, k)
        			k = next[k][j]
    			end
    			paths[i.node, j.node, i.orientation, j.orientation] = path
			end
		end
	end
	return costs, paths
end

function computePathsDP(neighs::Dict{State, Array{State, 1}}, costs::Array{Float64, 4}, interest_nodes::Array{Int64, 1})
	N = size(costs, 1)
	M = size(costs, 3)
	states = [State(n,phi) for n in 1:N for phi in 1:M if length(neighs[State(n, phi)]) > 0]
	filter!(s -> any(s in v for (k,v) in neighs), states)
	goals = filter(g -> g.node in interest_nodes, states)
	next = Dict(state => Dict(state => state) for state in states)
	for i in states
		costs[i.node, i.node, i.orientation, i.orientation] = 0.0
		for j in neighs[i]
			next[i][j] = j
		end
	end
	paths = fill(State[], (N,N,M,M))
	num_goals = length(goals)
	@showprogress 1 "PATH FIND: Each source from $num_goals goals..." for n in 1:length(states)
		for j in goals
    		for i in states
    			for k in neighs[i]			
    				if costs[i.node, k.node, i.orientation, k.orientation] + costs[k.node, j.node, k.orientation, j.orientation] < costs[i.node, j.node, i.orientation, j.orientation]
        				costs[i.node, j.node, i.orientation, j.orientation] = costs[i.node, k.node, i.orientation, k.orientation] + costs[k.node, j.node, k.orientation, j.orientation]
						next[i][j] = k
    				end
    			end
    		end
		end
	end
	@showprogress 1 "PATH FIND: Getting paths in list form..." for i in goals
		for j in goals
			if i != j
    			path = State[]
    			k = i
    			while k != j && haskey(next[k], j)
    				push!(path, k)
        			k = next[k][j]
    			end
    			paths[i.node, j.node, i.orientation, j.orientation] = path
			end
		end
	end
	return costs, paths
end

# function computePathsDPRecur(source::State, target::Source, neighs::Dict{State, Array{State, 1}}, costs::Array{Float64, 4})
	# if target in neighs[source]
		# return costs[source.node, target.node, source.orientation, target.orientation]
	# else
		# best = Inf
    	# for neigh in neighs
			# cumval = costs[source.node, target.node, source.orientation, target.orientation]
		# end
	# end
# end

function computePaths(neighs::Dict{State, Array{State, 1}}, costs::Array{Float64, 4}, interest_nodes::Array{Int64, 1})
	N = size(costs, 1)
	M = size(costs, 3)
	states = [State(n,phi) for n in 1:N for phi in 1:M if length(neighs[State(n, phi)]) > 0]
	filter!(s -> any(s in v for (k,v) in neighs), states)
	goals = filter(g -> g.node in interest_nodes, states)
	paths = fill(State[], (N,N,M,M))
	children = fill(State[], (N,N,M,M))

	@showprogress 1 "Goal to goal..." for goal in goals
		#goal = pop!(goals)
	Open = copy(states)
		#parents = Dict(u => goal for u in neighs[goal])
	paths[goal.node, goal.node, goal.orientation, goal.orientation] = [goal]
	for u in neighs[goal]
    	paths[goal.node, u.node, goal.orientation, u.orientation] = [goal; u]
	end
		#filter!(q -> !(q in Visited), Open)
    while length(Open) > 0
			qind = 1
			for i in eachindex(Open)
				if costs[goal.node, Open[i].node, goal.orientation, Open[i].orientation] < costs[goal.node, Open[qind].node, goal.orientation, Open[qind].orientation]
					qind = i
				end
			end
			q = Open[qind]
			deleteat!(Open, qind)
			for u in neighs[q]
				cumval = costs[goal.node, q.node, goal.orientation, q.orientation] + costs[q.node, u.node, q.orientation, u.orientation]
				if cumval < costs[goal.node, u.node, goal.orientation, u.orientation]
					costs[goal.node, u.node, goal.orientation, u.orientation] = cumval 
					#parents[u] = q
					paths[goal.node, u.node, goal.orientation, u.orientation] = push!(copy(paths[goal.node, q.node, goal.orientation, q.orientation]), u)
					#println(paths[goal.node, u.node, goal.orientation, u.orientation])
					#for pind in 2:length(paths[goal.node, u.node, goal.orientation, u.orientation])-1
						#p = paths[goal.node, u.node, goal.orientation, u.orientation][pind]
  					#costs[p.node, u.node, p.orientation, u.orientation] = cumval - costs[goal.node, p.node, goal.orientation, p.orientation]
  					#paths[p.node, u.node, p.orientation, u.orientation] = push!(copy(paths[p.node, q.node, p.orientation, q.orientation]), u)
					#end
				end
			end
			#if !(q in Visited)
    		#push!(Visited, q)
			#end
    end
		#for state in states
			#if !isequal(state, goal) && costs[goal.node, state.node, goal.orientation, state.orientation] < Inf
				#s = state
        #paths[state.node, state.node, state.orientation, state.orientation] = [state]
				#while haskey(parents, s) && !isequal(s, goal)
					#u = parents[s]
					#paths[u.node, state.node, u.orientation, state.orientation] = insert!(copy(paths[s.node, state.node, s.orientation, state.orientation]), 1, u)
					#s = u
				#end
				#insert!(paths[goal.node, state.node, goal.orientation, state.orientation], 1, goal)	
			#end
			#println(length(paths[goal.node, state.node, goal.orientation, state.orientation]))
		#end
		#filter!(g -> !(g in Visited), goals)
		#println(length(goals))
	end
	#println(costs)
	return costs, paths
end

function bestPaths(startState::State, Neighbors::Dict{State, Array{State, 1}}, Orientations, Costs::Array{Float64,4})
	N = size(Costs, 1)
	M = length(Orientations)
	#vals = fill(Inf, (N, M))
	paths = Dict{State, Array{State,1}}()
	Costs[startState.node, startState.node, startState.orientation, startState.orientation] = 0.0
	parents = Dict{State, State}()
	# Q = State[]
	# for i in Iterators.take(Iterators.countfrom(1), N)
	# 	for e in keys(Orientations)
	# 		push!(Q, State(i, e))
	# 	end
	# end
	Open = [startState]::Array{BuildStage.State, 1}
	Closed = State[]
	# println(length(Open))
	# push!(Open, startState)
	# expand to distant Neighbors
	# Openless(x, y) = vals[x.node, x.orientation] > vals[y.node, y.orientation] ? y : x
	while length(Open) > 0
		# println(parents)
		# println(Open)
		minindex = 1
		for i in eachindex(Open)
			if Costs[startState.node, Open[i].node, startState.orientation, Open[i].orientation] < Costs[startState.node, Open[minindex].node, startState.orientation, Open[minindex].orientation]
				minindex = i
			end
		end
		state = Open[minindex]
		deleteat!(Open, minindex)
		push!(Closed, state)
		# println(state, ", ", length(Open))
		for u in Neighbors[state]
			if !(u in Closed)
				if !(u in Open)
					push!(Open, u)
				end
  			v = Costs[startState.node, state.node, startState.orientation, state.orientation] + Costs[state.node, u.node, state.orientation, u.orientation]
				if v < Costs[startState.node, u.node, startState.orientation, u.orientation]
  				Costs[startState.node, u.node, startState.orientation, u.orientation] = v
  				parents[u] = state
  			end
			end
		end
	end
	# recover paths
	# println(startState)
	# println(parents)
	for i in 1:N, e in keys(Orientations)
		endState = State(i, e)
		if Costs[startState.node, endState.node, startState.orientation, endState.orientation] < Inf && !isequal(endState, startState)
			state = endState
			paths[endState] = [endState]
			while haskey(parents, state)
				state = parents[state]
				insert!(paths[endState], 1, state)
			end
			insert!(paths[endState], 1, startState)
		end
	end
	return Costs, paths
end

function checkInside(p, Polygon)
	const tol = 1e-1
	N = size(Polygon, 1)
	m(i) = i > N ? i - N : i
	clash = false
	for i in 1:N
		p1, p2 = Polygon[i], Polygon[m(i+1)]
		# inside the Y region
		if p1[2] - tol <= p[2] <= p2[2] + tol || p2[2] - tol <= p[2] <= p1[2] + tol
			if abs(p1[2] - p2[2]) < tol
				return true
			end
			xref = (p2[1] - p1[1])/(p2[2] - p1[2])*(p[2] - p1[2]) + p1[1]
			if abs(p[1] - p1[1]) < tol || abs(p[1] - p2[1]) < tol
				return true
			elseif p[1] <= xref
				clash = !clash
			end
		end
	end
	return clash
end

function checkInside(x, y, Polygon)
	const tol = 1e-6
	N = size(Polygon, 1)
	# m(i) = i > N ? i - N : i
	clash = false
	j = N
	for i in 1:N
		p1, p2 = Polygon[j], Polygon[i]
		# inside the Y region
		if (p1[2] < y <= p2[2] || p2[2] < y <= p1[2])
			if abs(p1[2] - p2[2]) < tol
				if (p1[1] < x <= p2[1] || p2[1] < x <= p1[1])
					return true
				elseif x <= p1[1] || x <= p2[1]
					clash = !clash
				end
			else
				xref = (p2[1] - p1[1])/(p2[2] - p1[2])*(y - p1[2]) + p1[1]
				if x <= xref
					clash = !clash
				end
			end
		end
		j = i
	end
	return clash
end

function bestGrid(boundaries, interests, step; porc_step=10, weight=1.0)
	smallest_x = minimum(map((x) -> x[1], boundaries))
	largest_x = maximum(map((x) -> x[1], boundaries))
	smallest_y = minimum(map((x) -> x[2], boundaries))
	largest_y = maximum(map((x) -> x[2], boundaries))
	grid_begin_x = smallest_x - step
	grid_end_x = smallest_x
	check_step_x = step*porc_step/100.0
	grid_begin_y = smallest_y - 0.5*step
	grid_end_y = smallest_y
	check_step_y = step*porc_step/100.0
	best = 0.0
	number = 0
	best_x = 0
	best_y = 0
	#@time
	lenx = length(grid_begin_x:check_step_x:grid_end_x)
	leny = length(grid_begin_y:check_step_y:grid_end_y)
	for begin_x in grid_begin_x:check_step_x:grid_end_x
		for begin_y in grid_begin_y:check_step_y:grid_end_y
			score = 0.0
			score_interest = Dict(k => 0.0 for k in keys(interests))
			counted = 0
			for x in begin_x:step:largest_x
				for y in begin_y:step:largest_y
					if checkInside(x,y, boundaries)
						score += 1.0
						counted += 1
					end
					if checkInside(x+step*0.5, y+step*0.5, boundaries)
						score += 1.0
						counted += 1
					end
					for (kint, vint) in interests
						if checkInside(x, y, vint["Boundaries"])
							score_interest[kint] += 1.0*weight
						end
						if checkInside(x+step*0.5, y+step*0.5, vint["Boundaries"])
							score_interest[kint] += 1.0*weight
						end
					end
				end
			end
			if score > best && all(v > 0.0 for v in values(score_interest))
				best = score
				number = counted
				best_x = begin_x
				best_y = begin_y
			end
		end
	end
	pos = zeros(number, 2)
	orientations = Dict(i => (i-1)*pi/4 for i in 1:8)
	neighs = Dict(State(i, b) => State[] for i in 1:number, b in keys(orientations))
	# println(neighs)
	i = 1
	#@time
	for y in best_y:step:largest_y
		for x in best_x:step:largest_x
			if checkInside(x, y, boundaries)
				pos[i, :] = [x, y]
				i += 1
			end
			if checkInside(x+0.5*step, y+0.5*step, boundaries)
				pos[i, :] = [x+0.5*step, y+0.5*step]
				i += 1
			end
		end
	end
	#@time
	cic(i) = i > 8 ? i - 8 : i < 1 ? 8 + i : i
	for i in 1:number
		for j in 1:number
			if i != j && norm(pos[j, :]-pos[i, :], 2) <= 1.3*step
				ang = mod(angle(Complex(pos[j, 1] - pos[i, 1], pos[j, 2] - pos[i, 2])), 2*pi)
				b = reduce((b1, b2) -> abs(orientations[b1] - ang) > abs(orientations[b2] - ang) ? b2 : b1, keys(orientations))
				append!(neighs[State(i, cic(b))], [State(j,cic(b-1)), State(j,cic(b)), State(j,cic(b+1))])
				append!(neighs[State(i, cic(b-1))], [State(j,cic(b-1)), State(j,cic(b)), State(j,cic(b+1))])
				append!(neighs[State(i, cic(b+1))], [State(j,cic(b-1)), State(j,cic(b)), State(j,cic(b+1))])
			#elseif i != j && norm(pos[j, :]-pos[i, :], 2) <= 1.3*step
				#ang = mod(angle(Complex(pos[j, 1] - pos[i, 1], pos[j, 2] - pos[i, 2])), 2*pi)
				#b = reduce((b1, b2) -> abs(orientations[b1] - ang) > abs(orientations[b2] - ang) ? b2 : b1, keys(orientations))
				#append!(neighs[State(i, cic(b))], [State(j,cic(b-1)), State(j,cic(b)), State(j,cic(b+1))])
				#append!(neighs[State(i, cic(b-1))], [State(j,cic(b-1)), State(j,cic(b)), State(j,cic(b+1))])
				#append!(neighs[State(i, cic(b+1))], [State(j,cic(b-1)), State(j,cic(b)), State(j,cic(b+1))])
			end
		end
	end
	return pos, orientations, neighs
end

function bestGridQuad(boundaries, interests, step; porc_step=10, weight=1.0)
	smallest_x = minimum(map((x) -> x[1], boundaries))
	largest_x = maximum(map((x) -> x[1], boundaries))
	smallest_y = minimum(map((x) -> x[2], boundaries))
	largest_y = maximum(map((x) -> x[2], boundaries))
	grid_begin_x = smallest_x - step
	grid_end_x = smallest_x
	check_step_x = step*porc_step/100.0
	grid_begin_y = smallest_y - 0.5*step
	grid_end_y = smallest_y
	check_step_y = step*porc_step/100.0
	best = 0.0
	number = 0
	best_x = 0
	best_y = 0
	#@time
	lenx = length(grid_begin_x:check_step_x:grid_end_x)
	leny = length(grid_begin_y:check_step_y:grid_end_y)
	for begin_x in grid_begin_x:check_step_x:grid_end_x
		for begin_y in grid_begin_y:check_step_y:grid_end_y
			score = 0.0
			score_interest = Dict(k => 0.0 for k in keys(interests))
			counted = 0
			for x in begin_x:step:largest_x
				for y in begin_y:step:largest_y
					if checkInside(x,y, boundaries)
						score += 1.0
						counted += 1
					end
					#if checkInside(x+step*0.5, y+step*0.5, boundaries)
						#score += 1.0
						#counted += 1
					#end
					for (kint, vint) in interests
						if checkInside(x, y, vint["Boundaries"])
							score_interest[kint] += 1.0*weight
						end
						#if checkInside(x+step*0.5, y+step*0.5, vint["Boundaries"])
							#score_interest[kint] += 1.0*weight
						#end
					end
				end
			end
			if score > best && all(v > 0.0 for v in values(score_interest))
				best = score
				number = counted
				best_x = begin_x
				best_y = begin_y
			end
		end
	end
	pos = zeros(number, 2)
	orientations = Dict(i => (i-1)*pi/4 for i in 1:8)
	neighs = Dict(State(i, b) => State[] for i in 1:number, b in keys(orientations))
	# println(neighs)
	i = 1
	#@time
	for y in best_y:step:largest_y
		for x in best_x:step:largest_x
			if checkInside(x, y, boundaries)
				pos[i, :] = [x, y]
				i += 1
			end
			#if checkInside(x+0.5*step, y+0.5*step, boundaries)
				#pos[i, :] = [x+0.5*step, y+0.5*step]
				#i += 1
			#end
		end
	end
	#@time
	cic(i) = i > 8 ? i - 8 : i < 1 ? 8 + i : i
	for i in 1:number
		for j in 1:number
			if i != j && norm(pos[j, :]-pos[i, :], 2) <= 1.7*step
				ang = mod(angle(Complex(pos[j, 1] - pos[i, 1], pos[j, 2] - pos[i, 2])), 2*pi)
				b = reduce((b1, b2) -> abs(orientations[b1] - ang) > abs(orientations[b2] - ang) ? b2 : b1, keys(orientations))
				# println(ang, ", ", b, ", ", orientations[b])
				append!(neighs[State(i, cic(b))], [State(j, b)])#[State(j,cic(b-1)), State(j,cic(b)), State(j,cic(b+1))])
				append!(neighs[State(i, cic(b-1))], [State(j, b), State(j, cic(b+1))])#[State(j,cic(b-1)), State(j,cic(b)), State(j,cic(b+1))])
				append!(neighs[State(i, cic(b+1))], [State(j, b), State(j, cic(b-1))])#[State(j,cic(b-1)), State(j,cic(b)), State(j,cic(b+1))])
			end
		end
	end
	return pos, orientations, neighs
end

function buildStage(specs; verbose=false, write_intermediate_file=false)
	boundaries = specs["Boundaries"]::Array{Array{Float64, 1}, 1}
	agents = specs["Agents"]
	num_agents = length(agents)
	num_operations = specs["Operations"]
	base_specs = specs["Base"]
	step = specs["Step"]
	if haskey(specs, "Obstacles")
		obstacles = specs["Obstacles"]
	else
		obstacles = Dict()
	end
	interest = specs["Interest"]
	efforts = Dict{Int64, Function}()
	for a in keys(agents)
		efforts[a] = eval(parse(agents[a]["effort"]))
	end

	if verbose
		println("Generating grid...")
	end

	if specs["Method"] == "OctoQuad"
		pos, orientations, neighsall = bestGrid(boundaries, interest, step, porc_step=10)
	elseif specs["Method"] == "Quad"
		pos, orientations, neighsall = bestGridQuad(boundaries, interest, step, porc_step=10)
	else
		throw("Segmentation Method not implemented")
		#return nothing
	end
	states = [State(i, b) for i in 1:size(pos, 1), b in keys(orientations) if length(neighsall[State(i, b)]) > 0]
	neighs = Dict(a => deepcopy(neighsall) for a in 1:num_agents)

	base_pos = base_specs["pos"]
	base_angle = deg2rad(base_specs["angle"])
	base_orientation = Complex(cos(base_angle), sin(base_angle))
	base_outbound = Dict(a => State(1,1) for a in 1:num_agents)
	base_inbound = Dict(a => State(1,1) for a in 1:num_agents)
	best_vals = Dict(a => Inf for a in 1:num_agents)
	# This section selects the best grid entering nodes, based on angles first and distance second
	cic(i) = i > length(orientations) ? i - length(orientations) : i < 1 ? length(orientations) + i : i
	dang(i) = abs(angle(Complex(pos[i, 1] - base_pos[1], pos[i, 2] - base_pos[2])/base_orientation))
	turnaround = fld(length(orientations), 2)
	nodesall = filter(i -> dang(i) < pi/6, [i for i in 1:size(pos, 1)])
	for a in 1:num_agents
		if length(obstacles) > 0
			obstacles_a = [obs["Boundaries"] for obs in values(obstacles) if a in obs["Agents"]]
			nodesa = filter(i -> !any(checkInside(pos[i, 1], pos[i, 2], obs) for obs in obstacles_a), nodesall)
		else
			obstacles_a = Int64[]
			nodesa = nodesall
		end
		distance, nodea = findmin(norm(Complex(pos[i, 1] - base_pos[1], pos[i, 2] - base_pos[2])) for i in nodesa)
		nodea = nodesa[nodea]
		e = reduce((e1, e2) -> abs(orientations[e1] - base_angle) > abs(orientations[e2] - base_angle) ? e2 : e1, keys(orientations))
		base_inbound[a] = State(nodea, e)
		base_outbound[a] = State(nodea, cic(e + turnaround))
		val_forward, _ = integrateEffort(efforts[a], buildBezier(base_pos, base_angle, State(nodea, e), agents[a]["minspeed"], pos, orientations), obstacles_a, boundaries, checkpoints=20)
		val_back, _ = integrateEffort(efforts[a], buildBezier(State(nodea, cic(e + turnaround)), base_pos, mod(base_angle+pi, 2*pi), agents[a]["minspeed"], pos, orientations), obstacles_a, boundaries, checkpoints=20)
		best_vals[a] = val_forward + val_back
	end

	gc()
	if verbose
		println("Computing neighboring costs...")
	end

	# println(base_entrance)
	# pos = [pos; [base_pos[1] base_pos[2]]]
	# base_n = size(pos, 1)
	# neighs[size(pos, 1)] = [base_entrance]
	# push!(neighs[base_entrance], size(pos, 1))


	num_nodes = size(pos, 1)
	num_states = length(states)
	num_orientations = length(orientations)
	nodecosts = zeros(num_nodes, num_operations)
	costs = fill(Inf, (num_nodes, num_nodes, num_orientations, num_orientations, num_agents))
	continous_paths = Dict{Tuple{State, State, Int64}, BezierPath}()
	for agent in 1:num_agents
		# obstacles for that agent
		obstacles_a = [obs["Boundaries"] for obs in values(obstacles) if agent in obs["Agents"]]
		@showprogress 2 "Computing for agent $agent..." for startState in states
			# delete from neighs the infinite values
			#to_delete = State[]
			for endState in neighs[agent][startState]
				if !isequal(startState, endState)
  				continous_paths[(startState, endState, agent)] = buildBezier(startState, endState, agents[agent]["minspeed"], pos, orientations)
  				cost, _ =	integrateEffort(efforts[agent], continous_paths[(startState, endState, agent)], obstacles_a, boundaries)
  				costs[startState.node, endState.node, startState.orientation, endState.orientation, agent] = cost
				else
					costs[startState.node, endState.node, startState.orientation, endState.orientation, agent] = 0.0
				end
			end
  		filter!(s -> costs[startState.node, s.node, startState.orientation, s.orientation, agent] < Inf, neighs[agent][startState])
		end
	end

	gc()
	if verbose
		println("Collecting interest nodes and demands...")
	end

	# println(costs)
	interest_nodes = Int64[]
	# supplies = Dict((a, o) => o in keys(agents[a]["operation"]) for a in 1:num_agents, o in 1:num_operations)
	for (narea, area) in interest
		inner_pos = Dict{Int64, Array{Float64, 1}}()
		for i in 1:size(pos, 1)
  		if checkInside(pos[i, 1], pos[i, 2], area["Boundaries"])
				inner_pos[i] = pos[i, :]
			end
		end
		total = length(inner_pos)
		nodes = collect(keys(inner_pos))
		for (kop, vop) in area["Demand"]
			for node in nodes
				nodecosts[node, kop] = Float64(vop)/Float64(total)
			end
		end
		append!(interest_nodes, nodes)
	end

	for a in 1:num_agents
		if !(base_inbound[a].node in interest_nodes)
			push!(interest_nodes, base_inbound[a].node)
		end
		if !(base_outbound[a].node in interest_nodes)
			push!(interest_nodes, base_outbound[a].node)
		end
	end
	# interest_and_base = [base_n; interest_nodes]

	gc()
	if verbose
		println("Constructing paths...")
	end

	# obstacles_nodes = Dict(a => Int64[] for a in 1:num_agents)
	# for agent in 1:num_agents
	# 	obstacles_a = filter((kobs, vobs) -> agent in vobs["Agents"], obstacles)
	# 	for node in 1:num_nodes
	# 		inside = any(map((obs) -> checkInside(pos[node, 1], pos[node, 2], obs["Boundaries"]), values(obstacles_a)))
	# 		if inside
	# 			push!(obstacles_nodes[agent], node)
	# 		end
	# 	end
	# end

	for a in 1:num_agents, (k, v) in neighs[a]
		for n in v
      		if costs[k.node, n.node, k.orientation, n.orientation, a] == Inf
				println("$k, $n")
				exit(0)
			end
		end
	end
	path_count = 0
	possible_states = filter(s -> s.node in interest_nodes, states)
	num_total_paths = length(possible_states)^2*num_agents
	allpaths = Array{Array{State,1},5}(num_nodes,num_nodes,num_orientations,num_orientations,num_agents)
	for a in 1:num_agents
      	costs[:,:,:,:,a], allpaths[:,:,:,:,a] = computePathsDP(neighs[a], costs[:,:,:,:,a], interest_nodes)
		gc()
	end
	#allpaths = Dict{Tuple{State, State, Int64}, Array{State, 1}}()
	#for agent in 1:num_agents
		#for startState in possible_states
			#println(startState)
			#println("remain: ", tomake - i)
			#costs[:, :, :, :, agent], paths = bestPaths(startState, neighs[agent], orientations, costs[:, :, :, :, agent])
			#println(vals)
			#println(paths)
			#for endState in possible_states
				#if rem(path_count, 1000) == 0
					#print("paths remaining: ", num_total_paths - path_count, " \r")
				#end
				#println(i, ", ", j, ", ", b, ", ", e, ", ", agent)
				#path_count += 1
			#end
		#end
	#end
	#println(costs)

	resDict = Dict{String, Any}()
	resDict["Nodes"] = size(interest_nodes, 1)
	resDict["Base"] = base_specs
	resDict["CoverRadius"] = specs["CoverRadius"]
	resDict["Step"] = specs["Step"]
	resDict["BaseInbound"] = base_inbound
	resDict["BaseOutbound"] = base_outbound
	resDict["Operations"] = num_operations
	resDict["Agents"] = agents
	resDict["Boundaries"] = boundaries
	resDict["Obstacles"] = obstacles
	# resDict["ObstaclesNodes"] = obstacles_nodes
	resDict["Interest"] = interest
	resDict["InterestNodes"] = interest_nodes
	resDict["Positions"] = pos
	resDict["Orientations"] = orientations
	resDict["NodeCosts"] = Dict(i => nodecosts[i, :] for i in interest_nodes)
	resDict["Costs"] = Dict(i =>
		Dict(j =>
			Dict(b =>
				Dict(e => costs[i, j, b, e, :] for e in keys(orientations))
			for b in keys(orientations))
		for j in interest_nodes)
	for i in interest_nodes)
	resDict["Paths"] = allpaths
	resDict["Neighs"] = neighs

	return resDict
end

end #module

#----------------------------
