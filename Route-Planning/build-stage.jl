if !(pwd() in LOAD_PATH)
    push!(LOAD_PATH, pwd())
end

using ImplementationTypes
using BuildStage
using Plots
using ArgParse
using YAML
using Utils

gr()
# pyplot()
#plotlyjs()

s = ArgParseSettings()

@add_arg_table s begin
	"--non-grid"
		help = "Generate instance in a grid, instead of free."
		action = :store_true
	"Specs"
		help = "Name of specs file for generation"
		arg_type = String
		default = "specs.yaml"
end

args = parse_args(s)

println("Reading specs...")

specs = YAML.load(open(args["Specs"]))

outDict = buildStage(specs, verbose=true)

println("Writing instance...")

open("instance-assign.yaml", "w") do f
	YAMLwrite(f, outDict)
end

println("Plotting...")

plotall, plotinterest, plotpaths = plotBuildStage(outDict, dpi=600)

savefig(plotall, "visual-segmentation.png")
savefig(plotinterest, "visual-segmentation-interest.png")
savefig(plotpaths, "visual-segmentation-interest-path.png")

println("Done.")
