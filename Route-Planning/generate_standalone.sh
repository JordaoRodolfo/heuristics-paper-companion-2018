echo -E "\documentclass[a4paper]{article}" > visual-standalone.tex
echo -E "\usepackage{pgfplots}" >> visual-standalone.tex
echo -E "\begin{document}" >> visual-standalone.tex
echo -E "\begin{figure}" >> visual-standalone.tex
echo -E "\centering" >> visual-standalone.tex
echo -E "\input{$1}" >> visual-standalone.tex
echo -E "\end{figure}" >> visual-standalone.tex
echo -E "\end{document}" >> visual-standalone.tex
