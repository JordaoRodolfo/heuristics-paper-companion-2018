import subprocess
import os

for f in os.listdir("."):
    if not "document" in f and "visual" in f and ".tex" in f:
        wrapped = "document-" + f
        with open(wrapped, "w") as out:
            out.write("\\documentclass{article}\n")
            out.write("\\usepackage{pgfplots}\n")
            out.write("\\begin{document}\n")
            out.write("\\begin{figure}\n")
            out.write("\\input{{{0}}}\n".format(f))
            out.write("\\end{figure}\n")
            out.write("\\end{document}\n")
        subprocess.run(["latexmk", "-pdf", wrapped])
        subprocess.run(["latexmk", "-c", wrapped])
