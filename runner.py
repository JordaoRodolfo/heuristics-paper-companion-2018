#!/usr/bin/python
import argparse
import subprocess
import os
import shutil
import glob

parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Run the stuff.

Examples:
  runner -rh 1-2 4 -rhm OsS
  'Runs the OsS method on specs 1 to 2 and 4'

  runner -re 1-3 5
  'Runs the solvers on specs 1 to 3 and 5'
""")
parser.add_argument('-re', type=str, nargs='+', help='Run exact routing')
parser.add_argument('-rer', type=str, nargs='+', help='Run exact relaxed model routing')
parser.add_argument('-rh', type=str, nargs='+', help='Heuristic routing')
parser.add_argument('-rhm', type=str, nargs='+', help='Heuristic routing methods')
parser.add_argument('-c', action='store_true', help='Run the control simu')

args = parser.parse_args()
if args.rh:
  print("----heuristics----")
  tests = []
  for h in args.rh:
    if "-" in h:
      nums = h.split("-")
      for n in range(int(nums[0]), int(nums[1])+1):
        if not n in tests:
          tests.append(n)
    else:
      n = int(h)
      if not n in tests:
        tests.append(n)
  tests.sort()
  methods = []
  if not args.rhm:
    methods.append("OsS")
  else:
    methods = args.rhm
  dest = os.path.join(os.getcwd(), "Route-Planning")
  for t in tests:
    l = "situation" + str(t)
    spec = l + "-specs.yaml"
    for m in methods:
      subprocess.Popen(["julia", "build-and-assign.jl", "-l", l, "--localMethod", m, spec], cwd=dest).wait()
  src = dest
  dest = os.path.join(os.getcwd(), "..")
  dest = os.path.join(dest, "Thesis")
  dest = os.path.join(dest, "chapters")
  dest = os.path.join(dest, "Route-Planning")
  dest = os.path.join(dest, "data")
  for f in glob.glob(os.path.join(src, "visual*"+l+".tex")):
    shutil.copy(f, dest)
if args.re:
  print("----exact full----")
  tests = []
  for h in args.re:
    if "-" in h:
      nums = h.split("-")
      for n in range(int(nums[0]), int(nums[1])+1):
        if not n in tests:
          tests.append(n)
    else:
      n = int(h)
      if not n in tests:
        tests.append(n)
  tests.sort()
  dest = os.path.join(os.getcwd(), "Route-Planning")
  for t in tests:
    l = "situation" + str(t)
    spec = l + "-specs.yaml"
    l += "-exact"
    subprocess.Popen(["julia", "build-and-assign.jl", "-l", l, "--exact", spec], cwd=dest).wait()
  src = dest
  dest = os.path.join(os.getcwd(), "..")
  dest = os.path.join(dest, "Thesis")
  dest = os.path.join(dest, "chapters")
  dest = os.path.join(dest, "Route-Planning")
  dest = os.path.join(dest, "data")
  for f in glob.glob(os.path.join(src, "visual*"+l+".tex")):
    shutil.copy(f, dest)
if args.rer:
  print("----exact relaxed----")
  tests = []
  for h in args.rer:
    if "-" in h:
      nums = h.split("-")
      for n in range(int(nums[0]), int(nums[1])+1):
        if not n in tests:
          tests.append(n)
    else:
      n = int(h)
      if not n in tests:
        tests.append(n)
  tests.sort()
  dest = os.path.join(os.getcwd(), "Route-Planning")
  for t in tests:
    l = "situation" + str(t)
    spec = l + "-specs.yaml"
    l += "-exact-relaxed"
    subprocess.Popen(["julia", "build-and-assign.jl", "-l", l, "--exact", "--relaxed", spec], cwd=dest).wait()
  src = dest
  dest = os.path.join(os.getcwd(), "..")
  dest = os.path.join(dest, "Thesis")
  dest = os.path.join(dest, "chapters")
  dest = os.path.join(dest, "Route-Planning")
  dest = os.path.join(dest, "data")
  for f in glob.glob(os.path.join(src, "visual*"+l+".tex")):
    shutil.copy(f, dest)
if args.c:
  print("-----model control-----")
  dest = os.path.join(os.getcwd(), "Modelling-Control")
  subprocess.Popen(["julia", "simulate_car.jl"], cwd=dest).wait()
  src = dest
  dest = os.path.join(os.getcwd(), "..")
  dest = os.path.join(dest, "Thesis")
  dest = os.path.join(dest, "chapters")
  dest = os.path.join(dest, "Modelling-Control")
  dest = os.path.join(dest, "data")
  for f in glob.glob(os.path.join(src, "*.tex")):
    shutil.copy(f, dest)
